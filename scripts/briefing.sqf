// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

player createDiaryRecord ["Diary", ["Changelog", "Version 1.1:<br/>
	- Removed the Ka-52<br/>
	- Added an unarmed Mi-8 transport helicopter<br/>
	- Rescaled the helicopter respawn timers<br/>
	- Fixed an issue regarding dynamic simulation at one of the objectives<br/>
	- Robot guns now use USB ports instead of triggers, and cannot be used by players.<br/><br/>
Version 1.0:<br/>
	- Initial release."]];

player createDiaryRecord ["Diary", ["Messages from the author(s)",
"""'Les Enfants Terribles'... Zero called it.""<br/>
				- Trenchgun<br/><br/>
""I hope you're stocked up on ammo for the airfield. Also, keep an eye out for laptops.""<br/>
				- Ghost of Officer O'Mally<br/><br/>
""Watch your step""<br/>
				- Charly<br/><br/>
""I'm so sorry""<br/>
				- Superxpdude<br/><br/>"]];

player createDiaryRecord ["Diary", ["Credits", "This mission would not have been possible without the contribution of the following individuals:<br/>
	- Trenchgun, Objectives<br/>
	- Ghost of Officer O'Mally, Objectives<br/>
	- Charly, Objectives<br/>
	- Sooty, Friendly loadouts and assets<br/>
	- Elynwyn, Briefing, task descriptions, and other lore<br/>
	- Superxpdude, Putting it all together<br/><br/>
This mission was a joint effort between six mission makers. Until the mission was duct-taped together, none of them knew what the others had prepared. Please be prepared for potential oddities during the op.
"]];

player createDiaryRecord ["Diary", ["Assets", "Your available assets for this mission are:<br/>
	- 1x Mi-24V Hind (15 minute respawn)<br/>
	- 1x Mi-8MTV-3 (10 minute respawn)<br/>
	- 1x Mi-8MT (5 minute respawn)<br/>
	- 1x Zamak Repair<br/>
	- 1x Zamak Rearm<br/>
	- 1x Zamak Refuel"
]];

player createDiaryRecord ["Diary", ["Mission",
"Your mission today is of utmost importance to our continued war efforts against NATO. <br/> <br/>
Intelligence reports that NATO has been pursuing two different methods of creating an infinite army with the help of an unknown private military organization. With their help, NATO has developed both a method to clone human beings, and to create robotic soldiers.<br/>
If either of these projects are allowed to leave the island, the war may as well be over. Your unit must find any data you can, and destroy it permanently. <br/> <br/>
In addition, there is a UN base south of the two experiment sites. Wipe them out once you've destroyed the experiments, leave no witnesses to our presence."]];
player createDiaryRecord ["Diary", ["Situation", "Satellite surveillance has indicated that NATO is experimenting on the northern coast on the island of Malden. Exact troop numbers are unknown due to the amount of covered structures in the AO.<br/><br/>
We expect that the experiment sites are heavily fortified, however they may not be prepared for a full on assault as trials are still underway."]];