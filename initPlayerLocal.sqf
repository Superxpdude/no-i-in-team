// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
[] execVM "scripts\briefing.sqf";

// Define the base sound path for custom sounds.
soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;

// Disable taking of robot clothes
player addEventHandler ["Take", {
    params ["_unitTaking", "_container", "_itemCN"];
    if (_itemCN == "H_RacingHelmet_4_F") then {
        _unitTaking unlinkItem _itemCN;
		_unitTaking removeItemFromUniform _itemCN;
		_unitTaking removeItemFromVest _itemCN;
		_unitTaking removeItemFromBackpack _itemCN;
		[_container, _itemCN] remoteExec ["addHeadgear", _container];
        systemChat "You can't take off the outer shell of a robot you dingus.";
    };
	if (_itemCN == "U_O_Protagonist_VR") then {
        _unitTaking unlinkItem _itemCN;
		[_container, _itemCN] remoteExec ["forceAddUniform", _container];
        systemChat "You can't take off the outer shell of a robot you dingus.";
    };
	if (_itemCN == "hlc_rifle_auga3_b") then {
        _unitTaking removeItem _itemCN;
		_unitTaking removeWeapon _itemCN;
		[_container, _itemCN] remoteExec ["addWeapon", _container];
        systemChat "The robot's weapons use USB instead of a trigger. You won't be able to use them.";
    };
}];