_this params ["_newVeh", "_currentturret"];

_currentmags = [""];
_currentweapons = [""];
switch (typeOf _newVeh) do 
{
	case "B_UGV_01_rcws_F": 
	{
		_currentmags = ["60Rnd_40mm_GPR_Tracer_Red_shells","60Rnd_40mm_GPR_Tracer_Red_shells","40Rnd_40mm_APFSDS_Tracer_Red_shells","200Rnd_127x99_mag_Tracer_Red"];
		_currentweapons = ["HMG_127_UGV", "autocannon_40mm_CTWS"];
	};
	
	case "B_T_UAV_03_dynamicLoadout_F": 
	{
		_currentmags = ["1000Rnd_20mm_shells","1000Rnd_20mm_shells"];
		_currentweapons = ["gatling_20mm"];
	};
};
{_newVeh removeWeaponTurret [_x, _currentturret] } forEach (_newVeh weaponsTurret _currentturret); 
{_newVeh addMagazineTurret [_x, _currentturret] } foreach _currentmags; 
{_newVeh addWeaponTurret [_x, _currentturret] } foreach _currentweapons;


