// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do {
	case "virus_uploaded": {
		if(!("hackMainframe" call BIS_fnc_taskCompleted)) then 
		{
			skynetTerminal setObjectTextureGlobal [0, "media\screens\bluescreen.paa"];
			playSound3D [soundPath + "media\mally_sound\errorDing.wav", skynetTerminal, false, getPosASL skynetTerminal, 10, 1, 120];
			{
				if(!(_x call BIS_fnc_taskCompleted)) then {[_x, "CANCELED"] call BIS_fnc_taskSetState;};
			} forEach ["hackSentinel", "hackFalcon", "hackStomper", "disableSAMs"];
			["hackMainframe", "SUCCEEDED"] call BIS_fnc_taskSetState;
			// Disable the remoteexecs that add holdactions
			{
				remoteExec ["", _x];
			} forEach ["mally_access_code_holdaction","mally_sam_holdaction","mally_skynet_holdaction","mally_sentinel_holdaction","mally_stomper_holdaction","mally_falcon_holdaction"];
			{
				_x setObjectTextureGlobal [0, "media\screens\laptopBluescreen.paa"];
				[_x, 0] call BIS_fnc_holdActionRemove;
				playSound3D [soundPath + "media\mally_sound\errorDing.wav", _x, false, getPosASL _x, 10, 1, 120];

			} forEach [sentinelLaptop, falconLaptop, stomperLaptop, codeLaptop, samLaptop];
			_robots = allUnits inAreaArray roboZone select {side _x == west}; 
			{
				vehicle _x lock true;
				_x setDamage 1; 
				sleep 0.2;
				playSound3D [soundPath + "media\mally_sound\errorDing.wav", _x, false, getPosASL _x, 10, 1, 120];
			} forEach (_robots /*call BIS_fnc_arrayShuffle*/);
			["beatRobots", "SUCCEEDED"] call BIS_fnc_taskSetState;
			
			// Spawn the check to see if the mission should end
			[] spawn {
				sleep 1;
				["mission_completed"] call SXP_fnc_updateTask;
			};
		};
	};
	
	case "uav_hacked": {
		if(!(_this select 2 call BIS_fnc_taskCompleted)) then 
		{
			_uavs = _this select 1;
			{
				_crew = crew _x;
				{_x enableAI "ALL";} forEach _crew; 
				_grp = createGroup east;
				_crew joinSilent _grp;
				group _x setBehaviour "AWARE";
			} forEach _uavs;
			[_this select 2, "SUCCEEDED"] call BIS_fnc_taskSetState;
		};
	};
	
	case "got_code": {
		if(!("getCode" call BIS_fnc_taskCompleted)) then 
		{
			["getCode", "SUCCEEDED"] call BIS_fnc_taskSetState;
			
			group falcon4 addWaypoint [position loiterBase1, 0];
			[group falcon4, 0] setWaypointType "MOVE";
			[group falcon4, 0] setWaypointCombatMode "RED";
			[group falcon4, 0] setWaypointBehaviour "COMBAT";
			group falcon4 addWaypoint [position loiterBase2, 1];
			[group falcon4, 1] setWaypointType "MOVE";
			group falcon4 addWaypoint [position loiterBase1, 2];
			[group falcon4, 2] setWaypointType "CYCLE";
			group falcon5 addWaypoint [position loiterBase4, 0];
			[group falcon5, 0] setWaypointType "MOVE";
			[group falcon5, 0] setWaypointCombatMode "RED";
			[group falcon5, 0] setWaypointBehaviour "COMBAT";
			group falcon5 addWaypoint [position loiterBase3, 1];
			[group falcon5, 1] setWaypointType "MOVE";
			group falcon5 addWaypoint [position loiterBase4, 2];
			[group falcon5, 2] setWaypointType "CYCLE";
		};
	};
	
	case "disable_sams": {
		if(!("disableSAMs" call BIS_fnc_taskCompleted)) then 
		{
			["disableSAMs", "SUCCEEDED"] call BIS_fnc_taskSetState;
			{
				vehicle _x lock true;
				_x setDamage 1; 
			} forEach [sam1, sam2, sam3];
			group falcon3 addWaypoint [position loiterRadio1, 0];
			[group falcon3, 0] setWaypointType "MOVE";
			[group falcon3, 0] setWaypointCombatMode "RED";
			[group falcon3, 0] setWaypointBehaviour "COMBAT";
			group falcon3 addWaypoint [position loiterRadio2, 1];
			[group falcon3, 1] setWaypointType "MOVE";
			group falcon3 addWaypoint [position loiterRadio3, 2];
			[group falcon3, 2] setWaypointType "MOVE";
			group falcon3 addWaypoint [position loiterRadio4, 3];
			[group falcon3, 3] setWaypointType "MOVE";
			group falcon3 addWaypoint [position loiterRadio1, 4];
			[group falcon3, 4] setWaypointType "CYCLE";
		};
	};
};

