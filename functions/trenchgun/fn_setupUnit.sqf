/*
	TRG_fnc_setupUnit
	Author: Trenchgun & Superxpdude
	Handles configuring units for trenchgun's objective.
	Code by Trenchgun, converted to function format by Superxpdude.
	
	Parameters:
		0: Object - Unit to configure
		1: String - Configuration to load
		
	Returns: Nothing
*/
// Define parameters
params [
	["_unit", nil, [objNull]],
	["_type", "default", [""]]
];

if (isNil "_unit") exitWith {};

switch (_type) do {
	case "skullface": {
		_unit setFace "mgsr_skullface"; 
		_unit setSpeaker "male12eng"; 
	};
	case "solid": {
		_unit setFace "mgsr_solid_face_ps1";
		_unit setSpeaker "ace_novoice";
	};
	default {
		_unit setFace "mgsr_nakedsnake";
		_unit setSpeaker "ace_novoice";
		
		// Only add the event handler from the server
		if (isServer) then {
			// MPKilled event handler to work nicely with the headless client
			_unit addMPEventHandler ["MPKilled", {
				// We don't need another Vauun sound locality event here
				if (isServer) then {
					_soundToPlay = soundPath + (["media\tg_sound\snd1.ogg","media\tg_sound\snd2.ogg"] call BIS_fnc_selectRandom);
					playSound3D [_soundToPlay, (_this select 0), false, getPosASL (_this select 0), 15, 1, 300];
					(_this select 0) removeAllMPEventHandlers "MPKilled";
				};
			}];
		};
	};
};