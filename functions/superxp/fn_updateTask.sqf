// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do {
	case "skullface": {
		["trg_kill_boss", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[] spawn {
			sleep 1;
			["trench_objective"] call SXP_fnc_updateTask;
		};
	};
	case "trench_computer": {
		["trg_clone_data", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[] spawn {
			sleep 1;
			["trench_objective"] call SXP_fnc_updateTask;
		};
	};
	case "trench_objective": {
		// Only proceed if all of Trenchgun's objectives have been completed.
		if (("trg_kill_boss" call BIS_fnc_taskCompleted) && ("trg_clone_data" call BIS_fnc_taskCompleted)) then {
			["trenchgun", "SUCCEEDED"] call BIS_fnc_taskSetState;
		};
		// Spawn the check to see if the mission should end
		[] spawn {
			sleep 1;
			["mission_completed"] call SXP_fnc_updateTask;
		};
	};
	
	case "chr_truck1": {
		["charly_truck1", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[] spawn {
			sleep 1;
			["charly_objective"] call SXP_fnc_updateTask;
		};
	};
	case "chr_truck2": {
		["charly_truck2", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[] spawn {
			sleep 1;
			["charly_objective"] call SXP_fnc_updateTask;
		};
	};
	case "chr_truck3": {
		["charly_truck3", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[] spawn {
			sleep 1;
			["charly_objective"] call SXP_fnc_updateTask;
		};
	};
	case "chr_officer": {
		["charly_officer", "SUCCEEDED"] call BIS_fnc_taskSetState;
		[[chr_demo1,chr_demo2,chr_demo3,chr_demo4,chr_demo5], -30, "#scripted"] call ace_explosives_fnc_scriptedExplosive;
		[] spawn {
			sleep 1;
			["charly_objective"] call SXP_fnc_updateTask;
		};
	};
	
	case "charly_objective": {
		// Only proceed if all of Trenchgun's objectives have been completed.
		if (("charly_truck1" call BIS_fnc_taskCompleted) && ("charly_truck2" call BIS_fnc_taskCompleted) && ("charly_truck3" call BIS_fnc_taskCompleted) && ("charly_officer" call BIS_fnc_taskCompleted)) then {
			["charly", "SUCCEEDED"] call BIS_fnc_taskSetState;
		};
		// Spawn the check to see if the mission should end
		[] spawn {
			sleep 1;
			["mission_completed"] call SXP_fnc_updateTask;
		};
	};
	
	
	case "mission_completed": {
		// Check to make sure that all top-level objectives have been completed
		if (("trenchgun" call BIS_fnc_taskCompleted) && ("beatRobots" call BIS_fnc_taskCompleted) && ("charly" call BIS_fnc_taskCompleted)) then {
			// If all of the top-level objectives have been completed. End the mission.
			["victory",true,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
		};
	};
};