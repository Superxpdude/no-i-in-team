// Server portion of fn_fixCurators
if (!isServer) exitWith {};

params ["_player","_playerUID"];
private _curatorList = [
	"76561198072893692", // Trenchgun
	"76561198022322970", // Charly
	"76561198043630557", // Lockmar
	"76561198017538703", // Sooty
	"76561198028100542", // OMally
	"76561198031434864"  // Superxpdude
];

if (_playerUID in _curatorList) then {
	[_player] spawn compile format [
		"unassignCurator zeus_module_%1;
		sleep 3;
		(_this select 0) assignCurator zeus_module_%1;",
		_playerUID
	];
};