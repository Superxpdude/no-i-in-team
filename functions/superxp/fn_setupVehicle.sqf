// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// Mi-8
	case (toLower "RHS_Mi8mt_vdv"): {
		// Configure the appearance
		[
			_newVeh,
			["standard",1], 
			["bench_hide",0,"RearDoors",0]
		] call BIS_fnc_initVehicle;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		// Add item cargo
		[_newVeh, "mi8"] call XPT_fnc_loadItemCargo;
	};
	// Mi-8MTV3
	case (toLower "RHS_Mi8MTV3_vdv"): {
		// Configure the appearance
		[
			_newVeh,
			["standard",1], 
			["bench_hide",0,"exhaust_hide",0,"RearDoors",0]
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh removeWeaponGlobal getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon");
			_newVeh removeWeaponTurret [getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon"), [-1]];
		} forEach getPylonMagazines _newVeh;
		
		// Configure pylons
		_newVeh setPylonLoadout ["pylon1", "rhs_mag_upk23_ofzt", false, []]; // Left inner
		_newVeh setPylonLoadout ["pylon2", "rhs_mag_upk23_ofzt", false, []]; // Right inner
		_newVeh setPylonLoadout ["pylon3", "rhs_mag_b8v20a_s8df", false, []]; // Left outer
		_newVeh setPylonLoadout ["pylon4", "rhs_mag_b8v20a_s8df", false, []]; // Right outer
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		// Add item cargo
		[_newVeh, "mi8"] call XPT_fnc_loadItemCargo;
	};
	// Mi-24
	case (toLower "RHS_Mi24V_vdv"): {
		// Configure the appearance
		[
			_newVeh,
			["standard",1], 
			true
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh removeWeaponGlobal getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon");
			_newVeh removeWeaponTurret [getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon"), [-1]];
		} forEach getPylonMagazines _newVeh;
		
		// Configure pylons
		_newVeh setPylonLoadout ["pylon1", "rhs_mag_b8v20a_s8kom", false, []]; // Left inner
		_newVeh setPylonLoadout ["pylon2", "rhs_mag_b8v20a_s8kom", false, []]; // Right inner
		_newVeh setPylonLoadout ["pylon3", "rhs_mag_b8v20a_s8df", false, []]; // Left middle
		_newVeh setPylonLoadout ["pylon4", "rhs_mag_b8v20a_s8df", false, []]; // Right middle
		_newVeh setPylonLoadout ["pylon5", "rhs_mag_9M120M_Mi24_2x", false, [0]]; // Left outer
		_newVeh setPylonLoadout ["pylon6", "rhs_mag_9M120M_Mi24_2x", false, [0]]; // Right outer
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		// Add item cargo
		[_newVeh, "mi24"] call XPT_fnc_loadItemCargo;
	};
	// Ka-52
	case (toLower "RHS_Ka52_vvsc"): {
		// Configure the appearance
		[
			_newVeh,
			["standard",1], 
			true
		] call BIS_fnc_initVehicle;
		
		{
			_newVeh removeWeaponGlobal getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon");
			_newVeh removeWeaponTurret [getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon"), [-1]];
		} forEach getPylonMagazines _newVeh;

		// Configure pylons
		_newVeh setPylonLoadout ["pylon1", "rhs_mag_b13l1_ka52_s13df", false, []]; //Left inner
		_newVeh setPylonLoadout ["pylon2", "rhs_mag_b13l1_ka52_s13df", false, []]; //Right inner
		_newVeh setPylonLoadout ["pylon3", "rhs_mag_apu6_9m127m_ka52", false, [0]]; //Left outer
		_newVeh setPylonLoadout ["pylon4", "rhs_mag_apu6_9m127m_ka52", false, [0]]; //Right outer
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		// Add item cargo
		[_newVeh, "ka52"] call XPT_fnc_loadItemCargo;
	};
};