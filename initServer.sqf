// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";



//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

// Define the base sound path for custom sounds.
soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;
publicVariable "soundPath";

[
	trg_computer, 
	"Delete Clone Data", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && !(""trg_clone_data"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""trg_clone_data"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["trench_computer"] remoteExec ["SXP_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	1000, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "trg_computer_holdaction"];


//-----------Mally Objective Code Start-----------

_robots = allUnits inAreaArray roboZone select {side _x == west};
{
[_x] call MLY_fnc_handleAIDamage;
} forEach _robots; //Make infantry (androids) take less damage

_uavs = allUnitsUAV inAreaArray roboZone;
{
	[vehicle _x] call MLY_fnc_handleAIDamage;
	if(typeOf _x == "B_UGV_01_rcws_F") then 
	{
		_x setObjectTextureGlobal [0, "media\textures\ugvskin1.jpg"]; 
		_x setObjectTextureGlobal [2, "media\textures\ugvskin2.jpg"]; 
	};
	if(typeOf _x == "B_T_UAV_03_dynamicLoadout_F") then 
	{
		_x setObjectTextureGlobal [0, "media\textures\uavskin1.jpg"];
		_x setObjectTextureGlobal [1, "media\textures\uavskin2.jpg"]; 
		group _x setBehaviour "CARELESS";
	};
} forEach _uavs; //Make UAVs take less damage and reskinning UAVs

{
	[_x, [0]] call MLY_fnc_addWeapons;
} forEach [falcon1, falcon2, falcon3, falcon4, falcon5]; //Adding weapons to falcon UAVs
{
	_x disableAI "ALL";
} forEach [falcon1, falcon2, sentinel, stomper1, stomper2]; //disabling AI for capturable UAVs

[
	codeLaptop, 
	"Download Access Codes", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && !(""getCode"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""getCode"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["got_code"] remoteExec ["MLY_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	0, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_access_code_holdaction"]; //Adding access code action

[
	samLaptop, 
	"Disable SAMs", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && !(""disableSAMs"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""disableSAMs"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["disable_sams"] remoteExec ["MLY_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	0, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_sam_holdaction"]; //Adding disable SAM action
 
[
	skynetTerminal, 
	"Delete System32", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && ""getCode"" call BIS_fnc_taskCompleted && !(""hackMainframe"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""hackMainframe"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["virus_uploaded"] remoteExec ["MLY_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	0, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_skynet_holdaction"]; //Adding mainframe destruction action

[
	sentinelLaptop, 
	"Hack Sentinel", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && ""getCode"" call BIS_fnc_taskCompleted && !(""hackSentinel"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""hackSentinel"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["uav_hacked", [sentinel], "hackSentinel"] remoteExec ["MLY_fnc_updateTask",2];},
	{}, 
	[], 
	8, 
	0, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_sentinel_holdaction"]; //Adding sentinel hacking action

[
	stomperLaptop, 
	"Hack Stompers", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && ""getCode"" call BIS_fnc_taskCompleted && !(""hackStomper"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""hackStomper"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["uav_hacked", [stomper1, stomper2], "hackStomper"] remoteExec ["MLY_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	0, 
	true,
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_stomper_holdaction"]; //Adding sentinel hacking action

[
	falconLaptop, 
	"Hack Falcons", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", 
	"_this distance _target < 5 && ""getCode"" call BIS_fnc_taskCompleted && !(""hackFalcon"" call BIS_fnc_taskCompleted)", 
	"_caller distance _target < 5 && !(""hackFalcon"" call BIS_fnc_taskCompleted)", 
	{}, 
	{}, 
	{["uav_hacked", [falcon1, falcon2], "hackFalcon"] remoteExec ["MLY_fnc_updateTask",2];}, 
	{}, 
	[], 
	8, 
	0, 
	true, 
	false
] remoteExec ["BIS_fnc_holdActionAdd", 0, "mally_falcon_holdaction"]; //Adding sentinel hacking action
 
 
//-----------Mally Objective Code End-----------