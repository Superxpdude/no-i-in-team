// Admin Console Access
// Admins defined within this file are given access to the "Mirage Coordinator" function within a mission.
// The ID used is the steam64ID of any players that are to have access to the console, in array format.

class cfgAdmins
{
	ids[] = {
		"76561198031434864", // Superxpdude
		"76561198028100542", // Ghost of Officer O'Mally
		"76561198017538703", // Sooty
		"76561198043630557", // Lockmar
		"76561198022322970", // Charly
		"76561198072893692"  // Trenchgun
	};
};