// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	class  rhs_vdv_flora_officer
	{
		displayName = "SquadOfficer"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_pp2000",  "rhs_pdu4"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"optic_Aco", "rhs_mag_9x19mm_7n31_44"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_altyn_novisor_ess_bala";
		facewearClass = "";
		vestClass = "rhs_vest_commander";
		backpackClass = "TFAR_mr3000_rhs";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemcTab", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_mag_rdg2_white", 3}, {"rhs_mag_rgd5", 1}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"rhs_mag_9x19mm_7n31_44", 4}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class rhs_vdv_flora_medic
	{
		displayName = "Medic"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_aks74un"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_dtk3", "rhs_acc_ekp1", "rhs_30Rnd_545x39_AK_green"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_flora";
		facewearClass = "";
		vestClass = "rhs_6b23_medic";
		backpackClass = "rhs_assault_umbts";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_30Rnd_545x39_AK_green", 8}, {"rhs_mag_rdg2_white", 4}, {"rhs_mag_rgd5", 2}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {{"ACE_fieldDressing", 40}, {"ACE_epinephrine", 10}, {"ACE_morphine", 10}, {"ACE_bloodIV_500", 15}}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	class cmd_medic: rhs_vdv_flora_medic
	{
		displayName = "Command Medic";
		backpackClass = "TFAR_mr3000_rhs";
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemcTab", "rhs_1PN138"};
	};
	class rhs_vdv_flora_engineer
	{
		displayName = "Engineer"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_aks74un","rhs_pdu4"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_dtk3", "rhs_acc_ekp1", "rhs_30Rnd_545x39_AK_green"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_flora";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh92";
		backpackClass = "rhs_assault_umbts_engineer_empty";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_30Rnd_545x39_AK_green", 6}, {"rhs_mag_an_m14_th3", 3}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"ACE_EntrenchingTool",1}, {"MineDetector",1}, {"ToolKit",1}, {"ACE_wirecutter",1}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
};
	class rhs_vdv_flora_grenadier
	{
		displayName = "Grenadier"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_ak103_gp25_npz"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_1p87", "hlc_75Rnd_762x39_m_rpk"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_green_ess_bala";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh92_vog";
		backpackClass = "rhs_sidor";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"ACE_EntrenchingTool",1}, {"rhs_VOG25", 6}, {"rhs_VG40OP_white", 2}, {"rhs_VG40OP_green", 2}, {"rhs_VG40OP_red", 2}, {"hlc_75Rnd_762x39_m_rpk", 2}} ; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"rhs_VOG25", 8}, {"rhs_VG40OP_white", 3}, {"rhs_VG40OP_green", 2}, {"rhs_VG40OP_red", 2}, {"rhs_30Rnd_762x39mm_polymer_tracer", 4}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class rhs_vdv_flora_machinegunner
	{
		displayName = "Autorifle"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_pkp"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_1p78", "CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_green_ess";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh116_vog_od";
		backpackClass = "rhs_assault_umbts";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M", 2}, {"rhs_mag_rdg2_white", 2}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M", 2}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class rhs_vdv_flora_at
	{
		displayName = "Anti Tank"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_ak103_2", "launch_O_Vorona_green_F"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_nita", "rhs_30Rnd_762x39mm_polymer_tracer"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {"Vorona_HEAT"}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_altyn_visordown";
		facewearClass = "";
		vestClass = "rhs_6b13_EMR_6sh92";
		backpackClass = "CUP_B_AlicePack_Khaki";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_mag_rdg2_white", 4}, {"rhs_30Rnd_762x39mm_polymer_tracer", 4}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"Vorona_HEAT", 3}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class rhs_vdv_flora_aa
	{
		displayName = "Anti Air"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_ak103_2", "rhs_weap_igla"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_nita", "rhs_30Rnd_762x39mm_polymer_tracer"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {"rhs_mag_9k38_rocket"}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_green_ess";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh116_od";
		backpackClass = "CUP_B_AlicePack_Khaki";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_mag_rdg2_white", 2}, {"rhs_30Rnd_762x39mm_polymer_tracer", 6}, {"rhs_mag_f1", 2}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"rhs_mag_9k38_rocket", 3}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	class rhs_vdv_flora_efreitor
	{
		displayName = "Team Leader"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_ak103_2", "rhs_weap_rpg26", "rhs_pdu4"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_nita", "rhs_acc_dtk3", "rhs_acc_perst3_top", "rhs_acc_grip_ffg2", "rhs_30Rnd_762x39mm_polymer_tracer"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_green_ess_bala";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh92_headset_mapcase";
		backpackClass = "B_Carryall_oli";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_30Rnd_762x39mm_polymer_tracer", 5}, {"rhs_mag_rdg2_white", 2}, {"rhs_mag_f1", 2}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"CUP_4Rnd_Igla_M", 1}, {"Vorona_HEAT", 1}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class rhs_vdv_flora_sergeant
	{
		displayName = "Squad Leader"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_ak103_zenitco01", "rhs_weap_rpg26", "rhs_pdu4"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_nita", "rhs_acc_dtk3", "rhs_acc_perst3_top", "rhs_acc_grip_ffg2", "rhs_30Rnd_762x39mm_polymer_tracer"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_vdv_flora";
		headgearClass = "rhs_6b28_green_ess_bala";
		facewearClass = "";
		vestClass = "rhs_6b23_6sh92_headset";
		backpackClass = "TFAR_mr3000_rhs";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemAndroid", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_30Rnd_762x39mm_polymer_tracer", 4}, {"rhs_mag_rdg2_white", 2}, {"SmokeShellRed", 2}, {"SmokeShellGreen", 2}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"rhs_30Rnd_762x39mm_polymer_tracer", 2}, {"rhs_mag_f1", 1}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
		
	};
	class O_helipilot_f
	{
		displayName = "Pilot"; // Currently unused, basically just a human-readable name for the loadout
		
		weapons[] = {"rhs_weap_pp2000"}; // Weapons for the unit, fills the primary weapon, launcher, pistol, and binocular slots
		primaryWeaponItems[] = {"rhs_acc_ekp8_18", "rhs_mag_9x19mm_7n21_44"}; // Primary weapon items. Includes magazine you want loaded initially
		secondaryWeaponItems[] = {}; // Secondary weapon items (launchers). Includes magazine you want loaded initially.
		handgunItems[] = {}; // Handgun items. Includes magazine you want loaded initially.
		
		uniformClass = "rhs_uniform_gorka_r_g";
		headgearClass = "H_PilotHelmetHeli_O";
		facewearClass = "";
		vestClass = "rhs_vydra_3m";
		backpackClass = "B_AssaultPack_khk";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemMicroDAGR", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
		uniformItems[] = {}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"rhs_mag_9x19mm_7n21_44", 4}, {"SmokeShellGreen", 2}, {"SmokeShellRed", 2}, {"SmokeShell", 2}, {"rhs_mag_f1", 1}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"ToolKit",1}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {{"ACE_fieldDressing", 20}, {"ACE_epinephrine", 5}, {"ACE_morphine", 5}, {"ACE_bloodIV_500", 2}}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only
	};
	class heli_lead: O_helipilot_f
	{
		displayName = "Lead Pilot"; // Currently unused, basically just a human-readable name for the loadout
		backpackClass = "TFAR_mr3000_rhs";
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_fadak", "ItemAndroid", "rhs_1PN138"}; // Linked items for the unit, use for map, compass, radio, watch, gps, and NVGs
	};
};