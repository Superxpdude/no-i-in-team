// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

/* 
	Randomization support
	
	--- Sub-class randomization --- 
	
	Any itemCargos class can define items through a sub-class instead of in the main class.
	In this case, the script will pick one of the sub-classes at random to apply to the object.
	The names of the sub-classes do not matter.
	
	The following example will select one of "box1" or "box2" when the "MyAmmoBox" class is called.
	The names of the subclasses don't matter, and they can be named whatever you want them to be.
	
	class itemCargos
	{
		class MyAmmoBox
		{
			class box1
			{
				items[] = {};
			};
			class box2
			{
				items[] = {};
			};
		};
	};
	
	
	--- Per-item randomization ---
	
	Any item can have the quantity changed from a fixed value, to a randomized value.
	By creating an array of three numbers instead of a single one, the itemCargo script will randomly select a quantity using the provided numbers.
	
	Example:
	
	class MyAmmoBox
	{
		items[] = {"FirstAidKit", {1,3,5}}; // Spawns a minimum of 1, a maximum of 5, and an average of 3
	};

*/
class itemCargos
{
	class example
	{
		// Array containing sub-arrays of items to add
		// Sub-arrays must include an item classname, and a quantity
		// The following would add 5 first aid kits to the inventory of the object
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE basic medical is being used
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE advanced medical is being used
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	
	class supplybox
	{
		items[] = {
			{"rhs_30Rnd_545x39_AK_green", 12},
			{"rhs_30Rnd_762x39mm_polymer_tracer", 40},
			{"hlc_75Rnd_762x39_m_rpk", 20},
			{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M", 12},
			{"Vorona_HEAT", 4},
			{"rhs_mag_9k38_rocket", 4},
			{"rhs_VOG25", 15},
			{"rhs_VG40OP_white", 5},
			{"rhs_VG40OP_green", 5},
			{"rhs_VG40OP_red", 5},
			{"SmokeShell", 10},
			{"SmokeShellGreen", 10},
			{"SmokeShellRed", 10},
			{"rhs_mag_rgd5", 10},
			{"rhs_mag_rdg2_white", 5},
			{"rhs_mag_an_m14_th3", 2},
			{"rhs_mag_f1",10}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 50},
			{"ACE_epinephrine", 15},
			{"ACE_morphine", 30},
			{"ACE_bloodIV_500", 5}
		};
		itemsAdvMed[] = {};
	};
	class mi8
	{
		items[] = {
			{"ToolKit", 2}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 120},
			{"ACE_bloodIV", 20},
			{"ACE_epinephrine", 50},
			{"ACE_morphine", 50},	
		};
		itemsAdvMed[] = {};
	};
	class mi24
	{
		items[] = {
			{"ToolKit", 2}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class ka52: mi24 {};
	class uavbox
	{
		items[] = {
			{"O_UavTerminal", 1}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
};