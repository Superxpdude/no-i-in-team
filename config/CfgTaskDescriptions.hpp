// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

// Trenchgun's objective master task
class trg
{
	title = "Attack of the Clones"; // Temp name
	description = "This site is primarily occupied by the PMC, eliminate them and their projects. ";
	marker = "";
};
class trg_clone_data
{
	title = "Destroy Cloning Data"; // Temp name
	description = "Hack their servers and expunge the data.";
	marker = "";
};
class trg_kill_boss
{
	title = "Kill the Boss"; // Temp name
	description = "Recent intelligence points to the owner of the PMC being on site, eliminate him.";
	marker = "";
};

// OMally's objectives
class mally_beatRobots
{
	title = "Shutting down Skynet"; // Temp name
	description = "This site is occupied by NATO's robots, cease their operations.";
	marker = "";
};
class mally_getCode
{
	title = "Get Access Codes"; // Temp name
	description = "It turns out that the NATO dogs are more secure in their operations than the PMC, find their access codes so we can shut down this operation.";
	marker = "";
};
class mally_hackMainframe
{
	title = "Hack Mainframe"; // Temp name
	description = "Hack their mainframe and delete the data after you've acquired the codes. We believe this will cause the robots to cease functioning entirely.";
	marker = "";
};
class mally_sams
{
	title = "Disable SAMs"; // Temp name
	description = "NATO is prepared for an air assault, disable their on site SAMs to clear the skies or assault from the ground instead.";
	marker = "";
};
class mally_sentinel
{
	title = "Hack Sentinel"; // Temp name
	description = "Recent satellite scans indicate offline Sentinels. Perhaps we can use them when we have the codes.";
	marker = "";
};
class mally_falcon
{
	title = "Hack Falcon"; // Temp name
	description = "Recent satellite scans indicate offline Falcons. Perhaps we can use them when we have the codes.";
	marker = "";
};
class mally_stomper
{
	title = "Hack Stompers"; // Temp name
	description = "Recent satellite scans indicate offline Stompers. Perhaps we can use them when we have the codes.";
	marker = "";
};

class charly_main
{
	title = "Clear the town"; // Temp name
	description = "The UN have set up a base within Larche. Eliminate their presence from the island, make sure they cannot leave.";
	marker = "";
};
class charly_truck1
{
	title = "Destroy the Truck"; // Temp name
	description = "The first truck is located in an area on the north side of Larche.";
	marker = "";
};
class charly_truck2
{
	title = "Destroy the Truck"; // Temp name
	description = "The second truck is located in an area on the south-west side of Larche.";
	marker = "";
};
class charly_truck3
{
	title = "Destroy the Truck"; // Temp name
	description = "The third truck is located in an area on the south-east side of Larche.";
	marker = "";
};
class charly_officer
{
	title = "Kill the Officer"; // Temp name
	description = "The officer is located somewhere within Larche.";
	marker = "";
};